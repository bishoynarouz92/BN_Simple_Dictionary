'use strict';

switch (process.argv[2]) {
    case 'list':
        const list = require('./Services/list');
        break;

    case 'add':
        const add = require('./Services/add');
        break;

    case 'get':
        const get = require('./Services/get');
        break;

    case 'remove':
        const remove = require('./Services/remove');
        break;

    case 'clear':
        const clear = require('./Services/clear');
        break;
}