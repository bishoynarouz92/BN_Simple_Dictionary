'use strict';
const fs = require('fs');

function get(key) {
    let list = fs.readFileSync('./Data/data.json');
    list = JSON.parse(list).data;
    let rowExists = false;
    for (let row of list) {
        if (row.key === key) {
            rowExists = row;
            break;
        }
    }
    if (rowExists) {
        console.log(rowExists);
    } else {
        console.log('Key Is not Exist');
    }
}

module.exports = get(process.argv[3]);
