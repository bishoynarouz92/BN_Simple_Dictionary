'use strict';
const fs = require('fs');

function list() {
    let list = fs.readFileSync('./Data/data.json');
    list = JSON.parse(list).data;
    if (list.length > 0) {
        for (let row of list) {
            console.log(row);
        }
    } else {
        console.log('there is no data to display');
    }
}

module.exports = list();
