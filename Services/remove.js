'use strict';
const fs = require('fs');

function remove(key) {
    let list = fs.readFileSync('./Data/data.json');
    list = JSON.parse(list).data;
    let isExist = false;

    for (let row of list) {
        if (row.key == key) {
            isExist = row;
            break;
        } else {
        }
    }
    if (isExist) {
        let index = list.indexOf(isExist);
        list.splice(index, 1);
        fs.writeFileSync('./Data/data.json', JSON.stringify({ data: list }));
        console.log('Row has been deleted Successfully');
    } else {
        console.log('Key is not Exist');
    }
}

module.exports = remove(process.argv[3]);


