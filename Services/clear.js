'use strict';
const fs = require('fs');

function clear() {
    fs.writeFileSync('./Data/data.json', JSON.stringify({ data: [] }));
    console.log('All Records have been deleted Successfully')
}

module.exports = clear();
