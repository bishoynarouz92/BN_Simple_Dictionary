'use strict';
const fs = require('fs');

function add(x, y) {
    let obj = { key: x, value: y };
    fs.readFile('./Data/data.json', 'utf-8', function (err, data) {
        let arrayOfObjects = JSON.parse(data);
        let isExists = false;
        for (let row of arrayOfObjects.data) {
            if (row.key === obj.key) {
                isExists = true;
                break;
            }
        }
        if (isExists) {
            console.log('Key is Already Exist');
        } else {
            arrayOfObjects.data.push(obj);
            fs.writeFile('./Data/data.json', JSON.stringify(arrayOfObjects), 'utf-8', function (err) {
                if (err) throw err
                console.log('New Record has been Inserted')
            })
        }
    })
}

module.exports = add(process.argv[3], process.argv[4]);
